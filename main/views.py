from django.shortcuts import render
from django.shortcuts import HttpResponse

# Create your views here.
def main_view2(request, *args, **kwargs):
    return HttpResponse("<h1>main</h1>")

def main_view(request, *args, **kwargs):
    return render(request, "utama.html", {})     #dictionary: context

