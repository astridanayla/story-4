from django.db import models

# Create your models here.

class Activity(models.Model):
    date = models.DateField(True, False)
    time = models.TimeField(True, False)
    location = models.CharField(max_length = 40)
    
    PERSONAL = 'personal'
    SOCIAL = 'social'
    ACADEMIC = 'academic'
    ORGANIZATIONAL = 'organizational'
    WORK = "work"
    CATEGORY_CHOICES = [
        (PERSONAL, 'Personal'),
        (SOCIAL, 'Social'),
        (ACADEMIC, 'Academic'),
        (ORGANIZATIONAL, 'Organizational'),
        (WORK, 'Work'),
    ]
    category = models.CharField(
        max_length=2,
        choices=CATEGORY_CHOICES,
        default=PERSONAL,
    )

    activity = models.CharField(max_length = 100)
