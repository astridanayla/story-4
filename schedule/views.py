from django.shortcuts import render, redirect
from .models import Activity
from .forms import ActivityForm
import datetime

# Create your views here.
def schedule_view(request, *args, **kwargs):
    my_form = ActivityForm()
    activities = Activity.objects.all()

    if request.method == 'POST':
        my_form = ActivityForm(request.POST)
        if my_form.is_valid():
            Activity.objects.create(**my_form.cleaned_data)
            return redirect('schedule')

    context = {
        'form' : my_form,
        'activities' : activities
    }
    return render(request, "schedule.html", context)  

def remove_schedule(request, id, *args, **kwargs):
    Activity.objects.filter(id=id).delete()

    my_form = ActivityForm()
    activities = Activity.objects.all()

    if request.method == 'POST':
        my_form = ActivityForm(request.POST)
        if my_form.is_valid():
            Activity.objects.create(**my_form.cleaned_data)

    context = {
        'form' : my_form,
        'activities' : activities
    }
    return render(request, "schedule.html", context)  
