from django import forms
from .models import Activity
import datetime

class ActivityForm(forms.Form):
    date = forms.DateField(initial=datetime.date.today)
    time = forms.TimeField(initial=datetime.time, widget=forms.TimeInput(format='%H:%M'))
    location = forms.CharField(max_length = 40)

    PERSONAL = 'personal'
    SOCIAL = 'social'
    ACADEMIC = 'academic'
    ORGANIZATIONAL = 'organizational'
    WORK = "work"
    CATEGORY_CHOICES = [
        (PERSONAL, 'Personal'),
        (SOCIAL, 'Social'),
        (ACADEMIC, 'Academic'),
        (ORGANIZATIONAL, 'Organizational'),
        (WORK, 'Work'),
    ]
    category = forms.ChoiceField(choices=CATEGORY_CHOICES)

    activity = forms.CharField(max_length = 100)