from django.shortcuts import render
from django.shortcuts import HttpResponse

# Create your views here.
def contact_view2(request, *args, **kwargs):
    print(args, kwargs)
    print(request)
    print(request.user)
    return HttpResponse("<h1>contact</h1>")

def contact_view(request, *args, **kwargs):
    return render(request, "contact.html", {})     #dictionary: context
